<?php
  class Employee extends Connection
  {
    public $id;
    public $firstname;
    public $lastname;
    public $address;
    public $city;
    public $country;
    public $phone;
    public $path;
    public $filename;


    public function setEmployeeId($id)
    {
      $this->id = $id;
    }


    public function getEmployeeId()
    {
      return $this->id;
    }


    public function setFirstName($fname)
    {
      $this->firstname = $fname;
    }


    public function getFirstName()
    {
      return $this->firstname;
    }


    public function setLastName($lname)
    {
      $this->lastname = $lname;
    }


    public function getLastName()
    {
      return $this->lname;
    }


    public function setAddress($add)
    {
      $this->address = $add;
    }


    public function getAddress()
    {
      return $this->address;
    }


    public function setCity($city)
    {
      $this->city = $city;
    }


    public function getCity()
    {
      return $this->city;
    }


    public function setCountry($country)
    {
      $this->country = $country;
    }


    public function getCountry()
    {
      return $this->country;
    }


    public function setPhone($phone)
    {
      $this->phone = $phone;
    }

    public function getPhone()
    {
      return $this->phone;
    }


    public function add_employee()
    {
      $this->sql = "INSERT INTO employees (first_name, last_name, address, city, country, phone) VALUES('$this->firstname', '$this->lastname', '$this->address', '$this->city', '$this->country', '$this->phone')";
      $this->res = mysqli_query($this->conn, $this->sql)
                        or die($this->error = mysqli_error($this->conn));
      $this->affRows = mysqli_affected_rows($this->conn);
      if($this->affRows>0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    public function getAllEmployees()
    {
      $this->sql = "SELECT * FROM employees";
      $this->res = mysqli_query($this->conn, $this->sql)
                        or die($this->error=mysqli_error($this->conn));
      $this->numRows = mysqli_num_rows($this->res);
      if($this->numRows>0)
      {
        while($row=mysqli_fetch_object($this->res))
        {
          array_push($this->data, $row);
        }
      }
      return $this->data;
    }


    public function delete_employee()
    {
      $this->sql = "DELETE FROM employees WHERE id='$this->id' ";
      $this->res = mysqli_query($this->conn, $this->sql)
                          or die($this->error=mysqli_error($this->conn));
      $this->affRows = mysqli_affected_rows($this->conn);
      if($this->affRows>0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    public function getEmployeeById()
    {
      $this->sql = "SELECT * FROM employees WHERE id='$this->id'";
      $this->res = mysqli_query($this->conn, $this->sql)
                    or die($this->error=mysqli_error($this->conn));
      $this->numRows = mysqli_num_rows($this->res);
      if($this->numRows>0)
      {
        while($row=mysqli_fetch_object($this->res))
        {
          array_push($this->data, $row);
        }
      return $this->data;
      }
    }

    /**
     * [update_employee description]
     * @return [type] [description]
     */
    public function update_employee()
    {
      $this->sql = "UPDATE employees SET
        first_name = '$this->firstname',
        last_name = '$this->lastname',
        address = '$this->address',
        city = '$this->city',
        phone = '$this->phone',
        country = '$this->country'
      WHERE id = '$this->id' ";

      $this->res = mysqli_query($this->conn, $this->sql)
                      or die($this->error=mysqli_error($this->conn));
      $this->affRows = mysqli_affected_rows($this->conn);
      if($this->affRows>0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    public function on_search($search)
  {
    $this->sql = "SELECT * FROM employees  WHERE CONCAT(first_name, last_name) LIKE '%".$search."%'";
    $this->res = mysqli_query($this->conn, $this->sql)
                    or die($this->error=mysqli_error($this->conn));
    $this->numRows = mysqli_num_rows($this->res);
    if($this->numRows>0)
    {
      while($row=mysqli_fetch_object($this->res))
      {
        array_push($this->data, $row);
      }
      return $this->data;
      // print_r($this->data);die;
    }
  }
  }