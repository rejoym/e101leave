<?php
class User extends Connection
{
	public $id;
	public $firstname;
	public $lastname;
	public $email;
	public $password;
	public $username;
	public $phone;


	public function setUserId($id)
	{
		$this->id = $id;
	}

	public function getUserId()
	{
		return $this->id;
	}

	public function setFirstName($fname)
	{
		$this->firstname = $fname;
	}


	public function getFirstName()
	{
		return $this->firstname;
	}

	public function setLastName($lname)
	{
		$this->lastname = $lname;
	}

	public function getLastName()
	{
		return $this->lastname;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setPassword($pass)
	{
		$this->password = $pass;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setUserName($uname)
	{
		$this->username = $uname;
	}

	public function getUserName()
	{
		return $this->username;
	}

	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * function to insert into users table
	 */
	public function addUser()
	{
		$this->sql = "INSERT INTO users(first_name, last_name, email, password, phone, username) VALUES('$this->firstname', '$this->lastname', '$this->email', '$this->password', '$this->phone', '$this->username')";
		$this->res = mysqli_query($this->conn, $this->sql)
										or die($this->error = mysqli_error($this->conn));
		$this->affRows = mysqli_affected_rows($this->conn);
		if($this->affRows>0)
			return true;
		else
			return false;
	}

	/**
	 * get all users
	 * @return [array] [description]
	 */
	public function getAllUsers()
	{
		$this->sql = "SELECT * FROM users";
		$this->res = mysqli_query($this->conn, $this->sql)
										or die($this->error = mysqli_error($this->conn));
		$this->numRows = mysqli_num_rows($this->res);
		if($this->numRows>0)
		{
			while($row = mysqli_fetch_object($this->res))
			{
				array_push($this->data, $row);
			}
		}
		return $this->data;
	}

	/**
	 * [checkUser description]
	 * @param  [type] $user [description]
	 * @param  [type] $pass [description]
	 * @return [type]       [description]
	 */
	public function checkUser($user, $pass)
	{
		$this->sql = "SELECT * FROM users WHERE email='$user' AND password='$pass'";
		$this->res = mysqli_query($this->conn, $this->sql)
											or die($this->error=mysqli_error($this->conn));
		$this->numRows = mysqli_num_rows($this->res);
		if($this->numRows>0)
		{
			while($row = mysqli_fetch_object($this->res))
			{
				array_push($this->data, $row);
			}
		}
		return $this->data;
	}

	/**
	 * function to delete a user
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete_user()
	{
		$this->sql = "DELETE FROM users WHERE id='$this->id' ";
		$this->res = mysqli_query($this->conn, $this->sql)
													or die($this->error=mysqli_error($this->conn));
		$this->affRows = mysqli_affected_rows($this->conn);
		if($this->affRows>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * function to get user by id
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getUserById($id)
	{
		$this->sql = "SELECT * FROM users WHERE id='$this->id'";
		$this->res = mysqli_query($this->conn, $this->sql)
										or die($this->error=mysqli_error($this->conn));
		$this->numRows = mysqli_num_rows($this->res);
		if($this->numRows>0)
		{
			while($row=mysqli_fetch_object($this->res))
			{
				array_push($this->data, $row);
			}
			return $this->data;
		}
	}

	/**
	 * function to update user
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function updateUser($id)
	{
		$this->sql = "UPDATE users SET
			first_name = '$this->firstname',
			last_name = '$this->lastname',
			email = '$this->email',
			username = '$this->username',
			phone = '$this->phone'
			WHERE id = '$this->id' ";

		$this->res = mysqli_query($this->conn, $this->sql)
											or die($this->error=mysqli_error($this->conn));
		$this->affRows = mysqli_affected_rows($this->conn);
		if($this->affRows>0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	public function updatePassword()
	{
		$this->sql = "UPDATE users SET password='$this->password'";
		$this->res = mysqli_query($this->conn, $this->sql)
										or die($this->error=mysqli_error($this->conn));
		$this->affRows = mysqli_affected_rows($this->conn);
		if($this->affRows>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}




}

