<?php
class Connection
{
  public $host;
  public $user;
  public $password;
  public $db;

  public $conn;
  public $error;
  public $sql;
  protected $res;
  protected $numRows;
  protected $affRows;

  public $data = [];


  public function __construct($db="e101leave", $h="localhost", $u="root", $p="password")
  {
    $this->host = $h;
    $this->user = $u;
    $this->password = $p;
    $this->db = $db;

    $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->db)
                          or die($this->error = mysqli_error($this->conn));
  }
}

?>