<?php

  require 'vendor/autoload.php';
  require 'Models/Redirect.class.php';

  $fb = new Facebook\Facebook([
    'app_id' => '375977509941279',
    'app_secret' => 'd2c3e9e8cda10c6218ed3ac2a4aad559',
    'default_graph_version' => '3.3',
  ]);

  $helper = $fb->getRedirectLoginHelper();
  $login_url = $helper->getLoginUrl("http://localhost/e101leave/?menu=login");

  try
  {
    $accessToken = $helper->getAccessToken();
    if(isset($accessToken))
    {
      $_SESSION['access_token'] = (string)$accessToken;
      new Redirect('?menu=home');
    }
  }
  catch(\Exception $ex)
  {
    echo $ex->getTraceAsString();
  }