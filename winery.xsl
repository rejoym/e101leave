<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html>
            <body>
                <h2>Winerys</h2>
                <table border="1">
                    <tr bgcolor="yellow">
                        <th align="left">ID</th>
                        <th align="left">First Name</th>
                        <th align="left">Last Name</th>
                        <th align="left">Address</th>
                        <th align="left">City</th>
                        <th align="left">Country</th>
                        <th align="left">Phone</th>
                    </tr>
                    <xsl:for-each select="winerys/winery">
                        <tr>
                            <td>
                                <xsl:value-of select="Id"/>
                            </td>
                            <td>
                                <xsl:value-of select="FirstName"/>
                            </td>
                            <td>
                                <xsl:value-of select="LastName"/>
                            </td>
                            <td>
                                <xsl:value-of select="Address"/>
                            </td>
                            <td>
                                <xsl:value-of select="City"/>
                            </td>
                            <td>
                                <xsl:value-of select="Country"/>
                            </td>
                            <td>
                                <xsl:value-of select="Phone"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>