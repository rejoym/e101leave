<div class="container">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Add Employees</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo BASE_URL; ?>">E101Leave</a></li>
                        <li><a href="<?php echo BASE_URL; ?>/register">Register</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="login-content"> -->
        <?php if(@$_SESSION['empty_fields']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger">Form Validation Error!!!</span>
                Please fill out all the fields.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['empty_fields']); ?>
        <?php if(@$_SESSION['insert_success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['insert_success']) ?></span>
                Record Inserted Successfully
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php unset($_SESSION['insert_success']); ?>
        <?php if(@$_SESSION['insert_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['insert_error']) ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['insert_error']); ?>
        <form action="Controllers/EmployeeController.php" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" name="input-firstname" placeholder="First Name">
                        <span class="my-error"><?php if(isset($_SESSION['error_firstname']) && empty($_POST['input-firstname'])){ echo "* Firstname Required"; unset($_SESSION['error_firstname']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" name="input-lastname" placeholder="Last Name">
                        <span class="my-error"><?php if(isset($_SESSION['error_lastname']) && empty($_POST['input-lastname'])){ echo "* Last Name Required"; unset($_SESSION['error_lastname']); }?></span>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="input-address" placeholder="Address">
                        <span class="my-error"><?php if(isset($_SESSION['error_address']) && empty($_POST['input-address'])){ echo "* Address Required"; unset($_SESSION['error_address']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" name="input-city" placeholder="City">
                        <span class="my-error"><?php if(isset($_SESSION['error_city']) && empty($_POST['input-city'])){ echo "* City Required"; unset($_SESSION['error_city']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="input-phone" placeholder="Phone">
                        <span class="my-error"><?php if(isset($_SESSION['error_phone']) && empty($_POST['input-phone'])){ echo "* Phone Required"; unset($_SESSION['error_phone']); }?></span>
                    </div>

                </div>
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label>Employee Image</label>
                        <input type="file" class="form-control" name="input-image">

                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" name="input-country" placeholder="Country">
                        <span class="my-error"><?php if(isset($_SESSION['error_country']) && empty($_POST['input-country'])){ echo "* Country Required"; unset($_SESSION['error_country']); }?></span>
                    </div>
                </div>
                </div>
            <div class="row">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="input-submit">Add Employees</button>
                </div>
            </div>

        </form>
    </div>
    <!-- </div> -->
</div>
