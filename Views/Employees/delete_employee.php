<?php
  require_once('Models/Connection.class.php');
  require_once('Models/Redirect.class.php');
  require_once('Models/Employee.php');

  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $objEmployee = new Employee();
  $objEmployee->setEmployeeId($id);

  $flag = $objEmployee->delete_employee();
  if($flag==true)
  {
    $_SESSION['delete_success'] = "Success!!";
    new Redirect('?menu=employees&action=list');
  }
  else
  {
    $_SESSION['delete_error'] = "Error deleting record";
    new Redirect('?menu=employees&action=list');
  }

  ?>