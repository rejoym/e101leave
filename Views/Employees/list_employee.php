<?php
  require_once('Models/Connection.class.php');
  require_once('Models/Employee.php');
  $employee = new Employee();
  $employee_list = $employee->getAllEmployees();
  ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <div class="card-header">
        <h4>Dashboard</h4>
      </div>
      <?php if(@$_SESSION['success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['success']) ?></span>
                Record Updated Successfully
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php unset($_SESSION['success']); ?>
        <?php if(@$_SESSION['error']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['delete_success']) ?></span>
                Record Deleted Successfully.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_success']); ?>
        <?php if(@$_SESSION['delete_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['delete_error']) ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_error']); ?>
      <?php if(isset($_SESSION['user_info'])) { ?>
      <div class="content mt-3">
        <div class="animated fadeIn">
          <div class="row">

            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong class="card-title">EMPLOYEE TABLE</strong>
                </div>
                <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>FIRST NAME</th>
                        <th>LAST NAME</th>
                        <th>ADDRESS</th>
                        <th>CITY</th>
                        <th>COUNTRY</th>
                        <th>PHONE NO</th>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                    <?php $sn = 0;
                          foreach ($employee_list as $employees) {
                     ?>
                        <td><?php echo ++$sn; ?></td>
                        <td><?php echo $employees->first_name; ?></td>
                        <td><?php echo $employees->last_name; ?></td>
                        <td><?php echo $employees->address; ?></td>
                        <td><?php echo $employees->city; ?></td>
                        <td><?php echo $employees->country; ?></td>
                        <td><?php echo $employees->phone; ?></td>
                        <td><a href="<?php echo BASE_URL; ?>?menu=employees&action=edit&id=<?php echo $employees->id; ?>"><i class="fa fa-edit"></i></a> | <a href="<?php echo BASE_URL; ?>?menu=employees&action=delete&id=<?php echo $employees->id; ?>"><i class="fa fa-trash-o"></i></a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>


          </div>
        </div><!-- .animated -->
      </div><!-- .content -->
    <?php } else { ?>
      <div class="row">
        <h3>Welcome to database test</h2>
      </div>
    <?php } ?>


    </div>
  </div>
</div>