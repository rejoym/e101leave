<div class="container">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Register</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo BASE_URL; ?>">E101Leave</a></li>
                        <li><a href="<?php echo BASE_URL; ?>/register">Register</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="login-content"> -->
        <?php //if(@$_SESSION['empty_fields']): ?>
            <!-- <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" id="error" style="display:none;">
                <span class="badge badge-pill badge-danger">Form Validation Error!!!</span>
                Please fill out all the fields.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
        <?php //endif; unset($_SESSION['empty_fields']); ?>
        <form method="post" id="register-validate">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" name="inputfirstname" placeholder="First Name">
                        <span class="my-error"><?php if(isset($_SESSION['error_firstname']) && empty($_POST['input-firstname'])){ echo "* Firstname Required"; unset($_SESSION['error_firstname']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" name="inputlastname" placeholder="Last Name">
                        <span class="my-error"><?php if(isset($_SESSION['error_lastname']) && empty($_POST['input-lastname'])){ echo "* Lastname Required"; unset($_SESSION['error_lastname']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="inputemail" placeholder="Email">
                        <span class="my-error"><?php if(isset($_SESSION['error_email']) && empty($_POST['input-email'])){ echo "* Email Required"; unset($_SESSION['error_email']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="inputpassword" placeholder="Password">
                        <span class="my-error"><?php if(isset($_SESSION['error_password']) && empty($_POST['input-password'])){ echo "* Password Required"; unset($_SESSION['error_password']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="inputusername" placeholder="Username">
                        <span class="my-error"><?php if(isset($_SESSION['error_username']) && empty($_POST['input-username'])){ echo "* Username Required"; unset($_SESSION['error_username']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="inputphone" placeholder="Phone Number">
                        <span class="my-error"><?php if(isset($_SESSION['error_phone']) && empty($_POST['input-phone'])){ echo "* Phone Required"; unset($_SESSION['error_phone']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="btn-submit" class="btn btn-primary btn-lg btn-block" name="input-submit">Register</button>
                </div>
            </div>

        </form><br>

        <div class="col-md-12">
            <h3 class="mb-3">Description</h3>
            <div class="jumbotron">
              On this page, as per the requirement, I have integrated a jQuery library called <strong>validate.js</strong> to check for form validation and also used ajax to send request asynchronously and get the response
          </div>
      </div>
    </div>

    <!-- </div> -->
</div>
