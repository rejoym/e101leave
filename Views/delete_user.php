<?php
  require_once('Models/Connection.class.php');
  require_once('Models/Redirect.class.php');
  require_once('Models/User.php');

  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $objUser = new User();
  $objUser->setUserId($id);

  $flag = $objUser->delete_user($id);
  if($flag==true)
  {
    $_SESSION['delete_success'] = "Success!!";
    new Redirect('?menu=test&action=database');
  }
  else
  {
    $_SESSION['delete_error'] = "Error deleting record";
    new Redirect('?menu=test&action=database');
  }

  ?>