<?php
    require_once('Models/Connection.class.php');
    require_once('Models/Redirect.class.php');
    require_once('Models/User.php');

    $user = new User();
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    //set the property
    $user->setUserId($id);
    $user_list = $user->getUserById($id);

    echo '<pre>';
    print_r($user_list);
    echo '</pre>';




?>
<div class="container">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Register</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo BASE_URL; ?>">E101Leave</a></li>
                        <li><a href="<?php echo BASE_URL; ?>/register">Register</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="login-content"> -->
        <?php if(@$_SESSION['empty_fields']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger">Form Validation Error!!!</span>
                Please fill out all the fields.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['empty_fields']); ?>
            <?php foreach($user_list as $users) { ?>
        <form action="Controllers/EditUserController.php" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" name="input-firstname" placeholder="First Name" value="<?php echo $users->first_name; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_firstname']) && empty($_POST['input-firstname'])){ echo "* Firstname Required"; unset($_SESSION['error_firstname']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" name="input-lastname" placeholder="Last Name" value="<?php echo $users->last_name; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_lastname']) && empty($_POST['input-lastname'])){ echo "* Lastname Required"; unset($_SESSION['error_lastname']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="input-email" placeholder="Email" value="<?php echo $users->email; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_email']) && empty($_POST['input-email'])){ echo "* Email Required"; unset($_SESSION['error_email']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="input-password" placeholder="Password">
                        <span class="my-error"><?php if(isset($_SESSION['error_password']) && empty($_POST['input-password'])){ echo "* Password Required"; unset($_SESSION['error_password']); }?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="input-username" placeholder="Username" value="<?php echo $users->username; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_username']) && empty($_POST['input-username'])){ echo "* Username Required"; unset($_SESSION['error_username']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="input-phone" placeholder="Phone Number" value="<?php echo $users->phone; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_phone']) && empty($_POST['input-phone'])){ echo "* Phone Required"; unset($_SESSION['error_phone']); }?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="hidden" name="input-id" value="<?php echo $users->id; ?>">
                </div>
            </div>
        <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="input-edit">Update</button>
                </div>
            </div>

        </form>
    </div>
    <!-- </div> -->
</div>
