<?php
  require_once('Models/Connection.class.php');
  require_once('Models/User.php');
  $user = new User();
  $user_list = $user->getAllUsers();
  ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <div class="card-header">
        <h4>DATABASE TEST</h4>
      </div>
      <?php if(@$_SESSION['update_success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['success']) ?></span>
                Password Updated Successfully
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php unset($_SESSION['update_success']); ?>
        <?php if(@$_SESSION['update_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['update_error']) ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['update_error']); ?>
      <?php if(@$_SESSION['success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['success']) ?></span>
                Record Updated Successfully
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php unset($_SESSION['success']); ?>
        <?php if(@$_SESSION['delete_success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['delete_success']) ?></span>
                Record Deleted Successfully.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_success']); ?>
        <?php if(@$_SESSION['delete_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['delete_error']) ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_error']); ?>
      <?php if(isset($_SESSION['user_info'])) { ?>
      <div class="content mt-3">
        <div class="animated fadeIn">
          <div class="row">

            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong class="card-title">USER TABLE</strong>
                </div>
                <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>FIRST NAME</th>
                        <th>LAST NAME</th>
                        <th>EMAIL</th>
                        <th>PASSWORD</th>
                        <th>USERNAME</th>
                        <th>PHONE NUMBER</th>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                    <?php $sn = 0;
                          foreach ($user_list as $users) {
                     ?>
                        <td><?php echo ++$sn; ?></td>
                        <td><?php echo $users->first_name; ?></td>
                        <td><?php echo $users->last_name; ?></td>
                        <td><?php echo $users->email; ?></td>
                        <td><?php echo $users->password; ?></td>
                        <td><?php echo $users->username; ?></td>
                        <td><?php echo $users->phone; ?></td>
                        <td><a href="<?php echo BASE_URL; ?>?menu=users&action=edit&id=<?php echo $users->id; ?>"><i class="fa fa-edit"></i></a> | <a href="<?php echo BASE_URL; ?>?menu=users&action=delete&id=<?php echo $users->id; ?>"><i class="fa fa-trash-o"></i></a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
                  <form action="Controllers/DatabaseController.php" method="post">
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="input-password" placeholder="Password" value="<?php echo $users->password; ?>">
                        <span class="my-error"><?php if(isset($_SESSION['error_psw']) && empty($_POST['input-password'])){ echo "* Password Required"; unset($_SESSION['error_psw']); }?></span>
                      </div>
                    </div>
                  </div>
                    <button class="btn btn-primary" name="update-password">Update Password</button>

                  </form>
            </div>


          </div>
        </div><!-- .animated -->
      </div><!-- .content -->
    <?php } else { ?>
      <div class="row">
        <h3>Welcome to database test</h2>
      </div>
    <?php } ?>


    </div>
  </div>
</div>