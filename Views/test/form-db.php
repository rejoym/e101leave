<?php
  require_once('Models/Connection.class.php');
  require_once('Models/Employee.php');
  $employee = new Employee();

  if(isset($_POST['input-submit']))
  {
    $to_search = $_POST['input-search'];
    $search_result = $employee->on_search($to_search);
  }
  else
  {
    $search_result = $employee->getAllEmployees();
  }
  ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">

      <div class="card-header">
        <h4>Form to DB Test</h4>
      </div>
      <?php if(@$_SESSION['success']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['success']) ?></span>
                Record Updated Successfully
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php unset($_SESSION['success']); ?>
        <?php if(@$_SESSION['error']): ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success"><?php print_r($_SESSION['delete_success']) ?></span>
                Record Deleted Successfully.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_success']); ?>
        <?php if(@$_SESSION['delete_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['delete_error']) ?></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['delete_error']); ?>
      <?php if(isset($_SESSION['user_info'])) { ?>
      <div class="content mt-3">
        <div class="animated fadeIn">
          <div class="row">

            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong class="card-title">EMPLOYEE TABLE</strong>
                </div>
                <div class="card-body">
                <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="input-search" placeholder="Search by First Name or Last Name">
                  </div>
                    <button class="btn btn-primary" name="input-submit">Search</button>
                </form><br>
                  <table  class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>FIRST NAME</th>
                        <th>LAST NAME</th>
                        <th>ADDRESS</th>
                        <th>CITY</th>
                        <th>COUNTRY</th>
                        <th>PHONE NO</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                    <?php $sn = 0;
                          foreach($search_result as $result) {
                     ?>
                        <td><?php echo ++$sn; ?></td>
                        <td><?php echo $result->first_name; ?></td>
                        <td><?php echo $result->last_name; ?></td>
                        <td><?php echo $result->address; ?></td>
                        <td><?php echo $result->city; ?></td>
                        <td><?php echo $result->country; ?></td>
                        <td><?php echo $result->phone; ?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>


          </div>
        </div><!-- .animated -->
      </div><!-- .content -->
    <?php } else { ?>
      <div class="row">
        <h3>Welcome to form-db  test</h2>
      </div>
    <?php } ?>


    </div>
  </div>
</div>