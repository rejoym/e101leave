<?php
// if (!$_SESSION['user_info']) {
  // session_start();
// }
 $GLOBALS['product_array'] = [
                                'id' => 243,
                                'description' => 'apples',
                                'type' => 'bramley'
                              ];
  // session_start();
  if(isset($_POST['input-submit1']))
    checkStringLength();
  elseif(isset($_POST['input-submit2']))
    changeToUpperCase();
  elseif(isset($_POST['input-submit3']))
    findStringPosition();
  elseif(isset($_POST['input-submit4']))
    encryptString();
  elseif(isset($_POST['input-submit5']))
    getDataType();
  elseif(isset($_POST['input-submit6']))
    checkNumeric();
  elseif(isset($_POST['input-submit7']))
    formatNumber();
  elseif(isset($_POST['input-submit8']))
    pushIntoArray();
  elseif(isset($_POST['input-submit9']))
    countArrayItems();
  elseif(isset($_POST['input-submit10']))
    checkInArray();
  elseif(isset($_POST['input-submit11']))
    addIntoArray();
  elseif(isset($_POST['input-submit12']))
    explodeArray();
  elseif(isset($_POST['input-submit13']))
    dateToday();
  elseif(isset($_POST['input-submit14']))
    calculateAge();
  elseif(isset($_POST['input-submit15']))
    sanitizeString();
  elseif(isset($_POST['input-submit16']))
    checkAlpha();

  // $array = ['id' => 1];
  /**
   * function to check length of the string
   * @return [type] [description]
   */
  function checkStringLength()
  {
    if(!empty($_POST['input-string']))
    {
      $length = strlen($_POST['input-string']);
      $_SESSION["result"] = $length;
    }
    else
    {
      $_SESSION["error"] = "Please enter a string";
    }
  }
  /**
   * function to convert the letters to uppercase
   * @return [type] [description]
   */
  function changeToUpperCase()
  {
    if(!empty($_POST['string-to-convert']))
    {
      $convert = strtoupper($_POST['string-to-convert']);
      $_SESSION["convert"] = $convert;
    }
    else
    {
      $_SESSION["error1"] = "Please enter a string";
    }
  }
  /**
   * [findStringPosition description]
   * @return [type] [description]
   */
  function findStringPosition()
  {
    $string_to_look = $_POST['string-to-find'];
    $string_query = $_POST['input-find-query'];
    if(!empty($string_to_look) && !empty($string_query))
    {
      $res = strpos($string_to_look, $string_query);
      $_SESSION["str-pos"] = $res;
    }
    else
    {
      $_SESSION["error3"] = "Please enter some string";
    }
  }

  /**
   * [encryptString description]
   * @return [type] [description]
   */
  function encryptString()
  {
    $to_encrypt = $_POST['input-encrypt'];
    if(!empty($to_encrypt))
    {
      $encrypt = md5($to_encrypt);
      $_SESSION['encrypt'] = $encrypt;
    }
    else
    {
      $_SESSION["error4"] = "Please enter a string";
    }
  }

  /**
   * [getDataType description]
   * @return [type] [description]
   */
  function getDataType()
  {
    $getvariable = $_POST['input-data'];
    if(!empty($getvariable))
    {
      if(filter_var($getvariable, FILTER_VALIDATE_INT))
        $_SESSION['data'] = "integer";
      elseif(filter_var($getvariable, FILTER_VALIDATE_FLOAT))
        $_SESSION['data'] = "float";
      else
        $_SESSION['data'] = "string";

    }
    else
    {
      $_SESSION['error5'] = "Please enter a string";
    }
  }

  /**
   * function to check whether inserted value is numeric or not
   * @return [type] [description]
   */
  function checkNumeric()
  {
    $getNumber = $_POST['input-numeric'];
    if(!empty($getNumber))
    {
      if(is_numeric($getNumber))
        $_SESSION['data1'] = "1 (true)";
      else
        $_SESSION['data1'] = "0 (false)";
    }
    else
    {
      $_SESSION['error6'] = "Please enter a string";
    }
  }

  /**
   * function to round up the given number to 1 decimal place
   * @return [type] [description]
   */
  function formatNumber()
  {
    $getDecimal = $_POST['input-decimal'];
    if(!empty($getDecimal))
    {
      if(!is_numeric($getDecimal))
      {
        $_SESSION['error7'] = "Please enter only numeric values";
      }
      else
      {
        $rounded_value = round($getDecimal, 1);
        $_SESSION['rounded'] = $rounded_value;
      }
    }
    else
    {
      $_SESSION['error7'] = "Please enter a string";
    }
  }

  /**
   * [pushIntoArray description]
   * @return [type] [description]
   */
  function pushIntoArray()
  {
    $id = $_POST['input-id'];
    $description = $_POST['input-description'];
    $type = $_POST['input-type'];
    // $product_array;
    if(empty($id) || empty($description) || empty($type))
    {
      $_SESSION['error8'] = "Please enter all the values";
    }
    else
    {
      $GLOBALS['product_array'] = [
        'id' => $id,
        'description' => $description,
        'type' => $type
      ];
      $_SESSION['array_result'] = $GLOBALS['product_array'];
    }
  }

  /**
   * [countArrayItems description]
   * @return [type] [description]
   */
  function countArrayItems()
  {
    $length = count($GLOBALS['product_array']);
    $_SESSION['array_length'] = $length;
  }

  /**
   * funciton to check whether a value exists in an array or not
   * @return [type] [description]
   */
  function checkInArray()
  {
    $string = $_POST['input-string'];
    if(empty($string))
    {
      $_SESSION['error9'] = "Please enter the string to search";
    }
    else
    {
      if(in_array($string, $GLOBALS['product_array']))
      {
        $_SESSION['search_value'] = "1 (true i.e. it exists)";
      }
      else
      {
        $_SESSION['search_value'] = "0 (false i.e. doesn't exist)";
      }
    }
  }

  /**
   * function to push into existing array
   * @param none
   */
  function addIntoArray()
  {
    $price = $_POST['input-price'];
    if(empty($price))
    {
      $_SESSION['error10'] = "Please enter price";
    }
    else
    {
      $to_push = ['price' => $price];
      $pushed = array_merge($GLOBALS['product_array'], $to_push);
      $_SESSION['pushed_array'] = $pushed;
    }
  }

  /**
   * [explodeArray description]
   * @return [type] [description]
   */
  function explodeArray()
  {
    $url = $_POST['input-url'];
    if(empty($url))
    {
      $_SESSION['error11'] = "Please enter URL";
    }
    else
    {
      $_SESSION['exploded_value'] = explode('.', $url);
    }
  }

  /**
   * [dateToday description]
   * @return [type] [description]
   */
  function dateToday()
  {
    $_SESSION['date'] = date('D jS F Y');
  }

  /**
   * function to calculate age
   * @return [type] [description]
   */
  function calculateAge()
  {
    $date = $_POST['input-birth'];
    if(empty($date))
    {
      $_SESSION['error12'] = "Please enter date";
    }
    else
    {
      $dob = new DateTime($date);
      $now = new DateTime();
      $difference = $now->diff($dob);
      $age = $difference->y;
      $_SESSION['age'] = $age;
    }
  }


  function sanitizeString()
  {
    if(empty($_POST['input-sanitize']))
    {
      $_SESSION['error13'] = "Please enter a string";
    }
    else
    {
      $string = $_POST['input-sanitize'];
      $_SESSION['sanitize'] = htmlspecialchars($string);
    }
  }


  function checkAlpha()
  {
    if(empty($_POST['input-check-string']))
    {
      $_SESSION['error14'] = "Please enter a string";
    }
    else
    {
      $string1 = $_POST['input-check-string'];
      if(ctype_alnum($string1))
      {
        $_SESSION['check-alpha'] = "The string $string1 consists of alphanumeric characters only";
      }
      else
      {
        $_SESSION['check-alpha'] = "Error!! The string $string1 doesn't consist of alphanumeric characters";
      }
    }
  }


?>
<h1>Built-in Functions</h1>
<hr>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Check</strong> String
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <div class="col-12 col-md-9">
            <label for="length-check">Enter string to check length of</label>
            <input type="text" class="form-control" placeholder="String eg. apple" id="length-check" name="input-string">
          </div>
          <span class="result"><?php echo isset($_SESSION["result"]) ? "The length of the string is:" .$_SESSION["result"] : @$_SESSION["error"]; ?></p></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit1">
        <i class="fa fa-dot-circle-o"></i> Check Length
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset1">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Change all characters</strong> to Uppercase
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <div class="col-12 col-md-9">
            <label for="to-convert">Enter string </label>
            <input type="text" class="form-control" name="string-to-convert" placeholder="String eg. apple" id="to-convert"
            value="<?php if(isset($_SESSION['convert']))
                            echo $_SESSION['convert'];
                    ?>">
          </div>
          <span class="result"><?php echo isset($_SESSION["error1"]) ? $_SESSION["error1"] : '' ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit2">
        <i class="fa fa-dot-circle-o"></i> Change to Uppercase
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset2">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Find position</strong> of string
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="find">Enter string to search in </label>
            <input type="text" class="form-control" name="string-to-find" placeholder="String eg. www.somesite.com" id="find">
            <label for="find-what">Enter what to find ?</label>
            <input type="text" class="form-control" id="find-what" name="input-find-query" placeholder="Eg. some">
          </div>

          <span class="result"><?php echo isset($_SESSION["str-pos"]) ? "The string starts at index " .$_SESSION["str-pos"] : @$_SESSION["error3"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit3">
        <i class="fa fa-dot-circle-o"></i> Find
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset2">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Encrypt</strong> a string with md5
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="string-to-encrypt">Enter string to encrypt </label>
            <input type="text" class="form-control" name="input-encrypt" placeholder="String eg. apple" id="string-to-encrypt">
          </div>

          <span class="result"><?php echo isset($_SESSION["encrypt"]) ? "The encrypted string is " .$_SESSION["encrypt"] : @$_SESSION["error4"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit4">
        <i class="fa fa-dot-circle-o"></i> Encrypt
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset3">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Get</strong> data type of a variable
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <div class="col-12 col-md-9">
            <label for="check-data">Enter data  </label>
            <input type="text" class="form-control" name="input-data" placeholder="Any data eg. 19" id="check-data">
          </div>

          <span class="result"><?php echo isset($_SESSION["data"]) ? "The data type of " .$_POST['input-data'] ." is " .$_SESSION["data"] : @$_SESSION["error5"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit5">
        <i class="fa fa-dot-circle-o"></i> Get data type
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Check</strong> whether a given number is numeric or not
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <div class="col-12 col-md-9">
            <label for="check-data">Enter any data  </label>
            <input type="text" class="form-control" name="input-numeric" placeholder="Any data eg. 27.579" id="check-data">
          </div>

          <span class="result"><?php echo isset($_SESSION['data1']) ? "The answer is " .$_SESSION['data1']  : @$_SESSION["error6"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit6">
        <i class="fa fa-dot-circle-o"></i> Check data type
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Format</strong> a given number to one decimal place
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="check-data">Enter any decimal number  </label>
            <input type="text" class="form-control" name="input-decimal" placeholder="Any decimal number eg. 27.579" id="check-data">
          </div>

          <span class="result"><?php echo isset($_SESSION['rounded']) ? "The formatted number is " .$_SESSION['rounded']  : @$_SESSION["error7"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm test" name="input-submit7">
        <i class="fa fa-dot-circle-o"></i> Format Number
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset7">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="card">
    <div class="card-header">
      <strong>Push</strong> into array, print the result and count the number of items in the array
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="id">Enter ID  </label>
            <input type="text" class="form-control" name="input-id" placeholder="Enter ID eg. 243" id="id" value="<?php echo isset($_POST['input-id']) ? $_POST['input-id'] : '' ?>">
          </div>
          <div class="col-12 col-md-9">
            <label for="description">Enter Description  </label>
            <input type="text" class="form-control" name="input-description" placeholder="Enter Description eg. Apple" id="description" value="<?php echo isset($_POST['input-description']) ? $_POST['input-description'] : '' ?>">
          </div>
          <div class="col-12 col-md-9">
            <label for="type">Enter Type  </label>
            <input type="text" class="form-control" name="input-type" placeholder="Enter Type eg. Bramley" id="description" value="<?php echo isset($_POST['input-type']) ? $_POST['input-type'] : ''  ?>">
          </div>
          <span class="result" style="width:100%"><?php isset($_SESSION['array_result']) ? print_r($_SESSION['array_result']) : @print_r($_SESSION['error8']) ?></span><br>
          <span class="result"><?php isset($_SESSION['array_length']) ? print_r("The number of items in the array is " .$_SESSION['array_length']) : @print_r($_SESSION['error8']) ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit8">
        <i class="fa fa-dot-circle-o"></i> Print the array</button>
‘      </button>
        <button type="submit" class="btn btn-success btn-sm" name="input-submit9">
        <i class="fa fa-tags"></i> Count the items in the array</button>
‘      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset8">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Check</strong> whether a given element is in the array or not
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="array-data">Enter any string  </label>
            <input type="text" class="form-control" name="input-string" placeholder="Any data eg. apples" id="array-data">
          </div>

          <span class="result"><?php echo isset($_SESSION['search_value']) ? "The result is " .$_SESSION['search_value']  : @$_SESSION["error9"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit10">
        <i class="fa fa-dot-circle-o"></i> Check string
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset9">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Add</strong> price in the array
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group custom-div">
          <div class="col-12 col-md-9">
            <label for="price">Enter Price  </label>
            <input type="text" class="form-control" name="input-price" placeholder="Price eg. 2.45" id="price">
          </div>

          <span class="result"><?php isset($_SESSION['pushed_array']) ? print_r($_SESSION['pushed_array'])  : @$_SESSION["error10"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit11">
        <i class="fa fa-dot-circle-o"></i> Add into array
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Explode</strong> url into array
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <div class="col-12 col-md-9">
            <label for="url">Enter URL  </label>
            <input type="text" class="form-control" name="input-url" placeholder="URL eg. www.somesite.com" id="url">
          </div>

          <span class="result"><?php isset($_SESSION['exploded_value']) ? print_r($_SESSION['exploded_value']) : @$_SESSION["error11"] ?></span>
        </div>
        <button type="submit" class="btn btn-primary btn-sm" name="input-submit12">
        <i class="fa fa-dot-circle-o"></i> Explode into array
      </button>
      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Get</strong> Today's date
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">

          <span class="result"><?php @print_r($_SESSION['date']) ?></span>
        </div>
          <button type="submit" class="btn btn-primary btn-sm" name="input-submit13">
            <i class="fa fa-dot-circle-o"></i> Get Today's Date
          </button>

      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Calculate</strong> Age
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <label for="birth-date">Enter Birth Date  </label>
          <input type="date" class="form-control" name="input-birth" placeholder="Birthdate" id="birth-date">
          <span class="result"><?php echo isset($_SESSION['age']) ? "The age is " .$_SESSION['age'] : @$_SESSION['error12'] ?></span>
        </div>
          <button type="submit" class="btn btn-primary btn-sm" name="input-submit14">
            <i class="fa fa-dot-circle-o"></i> Calculate Age
          </button>

      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Sanitize</strong> String
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <label for="string">Enter string  </label>
          <input type="text" class="form-control" name="input-sanitize" placeholder="String" id="string">
          <span class="result"><?php isset($_SESSION['sanitize']) ?  print_r($_SESSION['sanitize']) : print_r(@$_SESSION['error13']) ?></span>
        </div>
          <button type="submit" class="btn btn-primary btn-sm" name="input-submit15">
            <i class="fa fa-dot-circle-o"></i> Sanitize
          </button>

      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="card">
    <div class="card-header">
      <strong>Check</strong> String for alphanumeric characters only
    </div>
    <div class="card-body card-block">
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
        <div class="row form-group">
          <label for="string">Enter string  </label>
          <input type="text" class="form-control" name="input-check-string" placeholder="String" id="string">
          <span class="result"><?php isset($_SESSION['check-alpha']) ?  print_r($_SESSION['check-alpha']) : print_r(@$_SESSION['error14']) ?></span>
        </div>
          <button type="submit" class="btn btn-primary btn-sm" name="input-submit16">
            <i class="fa fa-dot-circle-o"></i> Check
          </button>

      <button type="reset" class="btn btn-danger btn-sm" name="input-reset4">
        <i class="fa fa-ban"></i> Reset
      </button>
      </form>
    </div>
  </div>
</div>
<script>

</script>
<?php

    //session_destroy();
    //$_SESSION = [];
?>