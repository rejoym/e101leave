<?php
  require_once('Models/Connection.class.php');
  if(isset($_POST['input-submit']))
  {
    $conn = new Connection();
    $conn->sql = "SELECT * FROM users";
    $result = mysqli_query($conn->conn, $conn->sql);

    $json_array = [];
    while($row=mysqli_fetch_array($result))
    {
      $json_array[] = $row;
    }
      $json_format = json_encode($json_array);

      $file_open = fopen("json_report.json", "w");
      $file_write = fwrite($file_open, $json_format);

      echo '<strong><a href="' .BASE_URL .'/json_report.json" style="color:red;font-size:20px">Click Here to Open JSON file</a></strong>';
  }

?>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <form method="post" action="">
          <button type="submit" class="btn btn-success" name="input-submit">Generate Data is JSON Format</button>
        </form>
      </div>
    </div>
  </div>
</div>