<?php
  require_once('Models/Connection.class.php');
  if(isset($_POST['input-submit']))
  {
    $conn = new Connection();
    $conn->sql = "SELECT * FROM users";
    $result = mysqli_query($conn->conn, $conn->sql);
    $xml = new DOMDocument("1.0");

    $xml->formatOutput = true;

    $users = $xml->createElement("users");
    $xml->appendChild($users);

    while($row=mysqli_fetch_array($result))
    {
      $user = $xml->createElement("user");
      $users->appendChild($user);

      $firstname = $xml->createElement("firstname", $row['first_name']);
      $user->appendChild($firstname);

      $lastname = $xml->createElement("lastname", $row['last_name']);
      $user->appendChild($lastname);

      $email = $xml->createElement("email", $row['email']);
      $user->appendChild($email);

      $username = $xml->createElement("username", $row['username']);
      $user->appendChild($username);

      $phone = $xml->createElement("phone", $row['phone']);
      $user->appendChild($phone);
    }
    echo "<xmp>" .$xml->saveXML(). "</xmp>";

    $xml->save("report_xml.xml");
  }

?>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <form method="post" action="">
          <button type="submit" class="btn btn-primary" name="input-submit">Generate XML</button>
        </form>
      </div>
    </div>
  </div>
</div>