<?php require 'fb_init.php'; ?>
<div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <?php if(@$_SESSION['empty_forms']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger">Form Validation Error!!!</span>
                Please fill out all the fields.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['empty_forms']); ?>

        <?php if(@$_SESSION['login_error']): ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger"><?php print_r($_SESSION['login_error']) ?></span>
                Please check your credentials and try again
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; unset($_SESSION['login_error']); ?>

                <div class="login-form">
                    <form action="Controllers/LoginController.php" method="post">
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="text" class="form-control" placeholder="Email" name="useroremail">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password">
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                        <div class="register-link m-t-15 text-center">
                            <p>Don't have account ? <a href="<?php echo BASE_URL;?>?menu=register"> Sign Up Here</a></p>
                        </div>
                        <button class="btn btn-primary"><a href="<?php echo $login_url; ?>"> Login With Facebook</a></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
