<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <form method="post">
          <button type="submit" class="btn btn-success" name="input-submit" id="btn-weather">Check Weather Forecast</button>

          <table id="weatherTable" class="table table-striped"></table>

          <p id="response"></p>
        </form>
      </div>
    </div>
  </div>
<div class="col-md-12">
            <h3 class="mb-3">Description</h3>
            <div class="jumbotron">
              On this page, as per the requirement, I have integrated an API call to <a href="openweathermap.org"><em>openweathermap.org</em></a> which gives information about the weather condition of different cities with extra information related to weather.
          </div>
      </div>
</div>

