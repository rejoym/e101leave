    <script src="<?php echo BASE_URL; ?>/public/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/plugins.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/main.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/lib/data-table/datatables-init.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/validate.js" type="text/javascript"></script>


    <script src="<?php echo BASE_URL; ?>/public/assets/js/dashboard.js"></script>
    <!--<script src="<?php //echo BASE_URL; ?>/public/assets/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="<?php //echo BASE_URL; ?>/public/assets/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="<?php //echo BASE_URL; ?>/public/assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="<?php //echo BASE_URL; ?>/public/assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>-->
    <script>
        // ( function ( $ ) {
        //     "use strict";

        //     jQuery( '#vmap' ).vectorMap( {
        //         map: 'world_en',
        //         backgroundColor: null,
        //         color: '#ffffff',
        //         hoverOpacity: 0.7,
        //         selectedColor: '#1de9b6',
        //         enableZoom: true,
        //         showTooltip: true,
        //         values: sample_data,
        //         scaleColors: [ '#1de9b6', '#03a9f5' ],
        //         normalizeFunction: 'polynomial'
        //     } );
        // } )( jQuery );
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
    <script src="<?php echo BASE_URL; ?>/public/assets/js/custom.js" type="text/javascript"></script>

</body>
</html>
