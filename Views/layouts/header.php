<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-101 Leave - Dashboard</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>/public/assets/css/custom.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/assets/scss/style.css">
    <link href="<?php echo BASE_URL; ?>/public/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="<?php echo BASE_URL; ?>/public/assets/images/logo.png" alt="Logo" width="50px"><span class="orange" style="color:#de3f18;font-weight: 800;">E-101&nbsp</span><span>Leave</span></a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>?menu=home"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <?php if(isset($_SESSION['user_info'])) {?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Employees</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus-square"></i><a href="<?php echo BASE_URL; ?>?menu=employees&action=add">Add Employees</a></li>
                            <li><i class="fa fa-th-list"></i><a href="<?php echo BASE_URL; ?>?menu=employees&action=list">List/Edit Employees</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-folder-open"></i>Leave Requests</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-th-list"></i><a href="#">View Leave Request</a></li>
                            <li><i class="fa fa-check-square"></i><a href="#">Approved Requests</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-umbrella"></i>Holidays</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="#">Add Holidays</a></li>
                            <li><i class="menu-icon fa fa-list-ul"></i><a href="#">List Holidays</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-microphone"></i>Announcements</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-square"></i><a href="#">Add Announcements</a></li>
                            <li><i class="menu-icon fa fa-th-list"></i><a href="#">List Announcements</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Attendance</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-list-alt"></i><a href="#">List Present Employees</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="#">List Absent Employees</a></li>
                        </ul>
                    </li>
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Test Conditions</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo BASE_URL; ?>?menu=test&action=initial">Initial Tests</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo BASE_URL; ?>?menu=test&action=function">Function Tests</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo BASE_URL; ?>?menu=test&action=database">Database Tests</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo BASE_URL; ?>?menu=test&action=form-db">Form to Database Tests</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart-o"></i>Report</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="<?php echo BASE_URL; ?>?menu=report&action=xml">Generate XML Document</a></li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="<?php echo BASE_URL; ?>?menu=report&action=json">Generate Report in JSON Format</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#"> <i class="menu-icon fa fa-calendar-o"></i>View Calendar </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>?menu=api&action=weather"> <i class="menu-icon fa fa-calendar-o"></i>API Call </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>?menu=gallery"> <i class="menu-icon fa fa-user"></i>Gallery </a>
                    </li>

                <?php } else { ?>
                     <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Test Conditions</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo BASE_URL; ?>?menu=test&action=function">Function Tests</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>?menu=login"> <i class="menu-icon fa fa-user"></i>Login </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>?menu=register"> <i class="menu-icon fa fa-dashboard"></i>Register </a>
                    </li>
                <?php } ?>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form" method="post" action="Controllers/GoogleSearchController.php">
                                <input class="form-control mr-sm-2" name="query" type="text" placeholder="Search on Google..." aria-label="Search">
                                <button type="submit">Search</button>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="<?php echo BASE_URL; ?>/public/assets/images/admin.jpg" alt="User Avatar">
                            <small>Welcome, <?php echo isset($_SESSION['user_info']) ? $_SESSION['user_info'][0]->username : ''?></small>
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa-user"></i>&nbsp;My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa-cog"></i>&nbsp;Settings</a>

                                <a class="nav-link" href="<?php echo BASE_URL ?>/?menu=logout"><i class="fa fa-power-off"></i>&nbsp;Logout</a>
                        </div>
                    </div>

                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->