<?php
session_start();
  require_once('../Models/Connection.class.php');
  require_once('../Models/User.php');
  require_once('../Models/Redirect.class.php');

  $objUser = new User();

    if(isset($_POST['input-edit']))
    {
        if(empty($_POST['input-firstname']) || empty($_POST['input-lastname']) || empty($_POST['input-email']) || empty($_POST['input-username']) || empty($_POST['input-phone']))
        {
            $_SESSION['empty_fields'] = "Please enter all the details";
            new Redirect('../?menu=users&action=edit');
        }
        else
        {
            $id = $_POST['input-id'];
            $fname = trim($_POST['input-firstname']);
            $lname = trim($_POST['input-lastname']);
            $email = trim($_POST['input-email']);
            $uname = trim($_POST['input-username']);
            $phone = trim($_POST['input-phone']);

            $objUser->setFirstName($fname);
            $objUser->setLastName($lname);
            $objUser->setEmail($email);
            $objUser->setUserName($uname);
            $objUser->setPhone($phone);
            $objUser->setUserId($id);

            $edit_flag = $objUser->updateUser($id);
            if($edit_flag == true)
            {
                $_SESSION['success'] = "Success!!";
                new Redirect('../?menu=test&action=database');
            }
            else
            {
              // echo "string";die;
                $_SESSION['error'] = "Error updating record";
                new Redirect('../?menu=users&sction=edit&error=error');
            }
        }
    }
?>