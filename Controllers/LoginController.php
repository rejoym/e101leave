<?php
  session_start();
  require_once('../Models/Connection.class.php');
  require_once('../Models/User.php');
  require_once('../Models/Redirect.class.php');

  $objUser = new User();
  $useroremail = $password = "";
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
    if(empty($_POST['useroremail']) || empty($_POST['password']))
    {
      $_SESSION['empty_forms'] = "Please fill out all the details";
      $_SESSION['empty_user'] = "Please enter username";
      $_SESSION['empty_password'] = "Please enter password";
      new Redirect('../?menu=login');
    }
    else
    {
      $useroremail = sanitize($_POST['useroremail']);
      $password = sanitize($_POST['password']);

      $check = $objUser->checkUser($useroremail, $password);
      if($check)
      {
        $_SESSION['user_info'] = $check;
        new Redirect('../?menu=home');
      }
        else
        {
          $_SESSION['login_error'] = 'Invalid credentials';
          new Redirect('../?menu=login');
        }
      }
    }



  function sanitize($data)
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }