<?php
  session_start()
  require_once('../Models/Connection.class.php');
  require_once('../Models/Redirect.class.php');
  require_once('../Models/Employee.php');

  $employee = new Employee();

  if(isset($_POST['input-submit']))
  {
    $to_search = $_POST['input-search'];
    $_SESSION['search_result'] = $employee->on_search($to_search);

  }
  else
  {
    $_SESSION['search_result'] = $employee->getAllUsers();
  }

?>