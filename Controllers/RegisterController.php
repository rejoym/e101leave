	<?php
	session_start();
	require_once('../Models/Connection.class.php');
	require_once('../Models/User.php');
	require_once('../Models/Redirect.class.php');

	$objUser = new User();

	if(isset($_POST['input-submit']))
	{
		$fname = mysqli_real_escape_string($objUser->conn, $_POST['inputfirstname']);
		$lname = mysqli_real_escape_string($objUser->conn, $_POST['inputlastname']);
		$email = mysqli_real_escape_string($objUser->conn, $_POST['inputemail']);
		$pass = mysqli_real_escape_string($objUser->conn,  $_POST['inputpassword']);
		$uname = mysqli_real_escape_string($objUser->conn,  $_POST['inputusername']);
		$phone = mysqli_real_escape_string($objUser->conn, $_POST['inputphone']);
		// if(empty($_POST['inputfirstname']) || empty($_POST['inputlastname']) || empty($_POST['inputemail']) || empty($_POST['inputpassword']) || empty($_POST['inputusername']))
		// {
		// 	$_SESSION['error_firstname'] = "Please enter First Name";
		// 	$_SESSION['error_lastname'] = "Please enter Last Name";
		// 	$_SESSION['error_email'] = "Please enter email";
		// 	$_SESSION['error_password'] = "Please enter password";
		// 	$_SESSION['error_username'] = "Please enter username";
		// 	$_SESSION['error_phone'] = "Please enter phone no";
		// 	$_SESSION['empty_fields'] = "Please fill out all the details";
		// 	new Redirect('?menu=register');
		// }
		// else
		// {
			$objUser->setFirstName($fname);
			$objUser->setLastName($lname);
			$objUser->setEmail($email);
			$objUser->setPassword($pass);
			$objUser->setUserName($uname);
			$objUser->setPhone($phone);
			// print_r(BASE_URL);die;
			$check = $objUser->addUser();
			if($check)
				return json_encode($check);
			else
				new Redirect('?menu=register');
		// }
	}




	?>