<?php
  require_once('../Models/Connection.class.php');
  require_once('../Models/Redirect.class.php');
  require_once('../Models/User.php');


  $user = new User();

  if(isset($_POST['update-password']))
  {
    if(empty($_POST['input-password']))
    {
      $_SESSION['error_update'] = "Please fill the password field";
      new Redirect('../?menu=test&action=database');
    }
    else
    {
      $pass = trim($_POST['input-password']);
      $user->setPassword($pass);
      $update_flag = $user->updatePassword();
      if($update_flag == true)
      {
        $_SESSION['update_success'] = "Success";
        new Redirect('../?menu=test&action=database');
      }
      else
      {
        $_SESSION['update_error'] = "Error";
        new Redirect('../?menu=test&action=database');
      }

    }
  }