<?php
  session_start();
  require_once('../Models/Connection.class.php');
  require_once('../Models/Redirect.class.php');
  require_once('../Models/Employee.php');

  $employee = new Employee();

  if(isset($_POST['input-submit']))
  {
    $fname = mysqli_real_escape_string($employee->conn, $_POST['input-firstname']);
    $lname = mysqli_real_escape_string($employee->conn, $_POST['input-lastname']);
    $address = mysqli_real_escape_string($employee->conn, $_POST['input-address']);
    $city = mysqli_real_escape_string($employee->conn,  $_POST['input-city']);
    $country = mysqli_real_escape_string($employee->conn,  $_POST['input-country']);
    $phone = mysqli_real_escape_string($employee->conn, $_POST['input-phone']);
    if(empty($_POST['input-firstname']) || empty($_POST['input-lastname']) || empty($_POST['input-address']) || empty($_POST['input-city']) || empty($_POST['input-country']) || empty($_POST['input-phone']))
    {
      $_SESSION['error_firstname'] = "Please enter First Name";
      $_SESSION['error_lastname'] = "Please enter Last Name";
      $_SESSION['error_address'] = "Please enter email";
      $_SESSION['error_city'] = "Please enter password";
      $_SESSION['error_country'] = "Please enter username";
      $_SESSION['error_phone'] = "Please enter phone no";
      $_SESSION['empty_fields'] = "Please fill out all the details";
      new Redirect('../?menu=employees&action=add');
    }
    else
    {
      $employee->setFirstName($fname);
      $employee->setLastName($lname);
      $employee->setAddress($address);
      $employee->setCity($city);
      $employee->setCountry($country);
      $employee->setPhone($phone);
      // print_r(BASE_URL);die;
      $check = $employee->add_employee();
      if($check) {
        $_SESSION['insert_success'] = "Success";
        new Redirect('../?menu=employees&action=add');
      }
      else
      {
        $_SESSION['insert_error'] = "Error";
        new Redirect('../?menu=employees&action=add');
      }
    }
  }





?>