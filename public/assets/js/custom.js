$('document').ready(function() {

  /* handle form validation */
  $("#register-validate").validate({
    rules:
    {
      inputfirstname: {
        required: true,
        minlength: 3
      },
      inputlastname: {
        required: true,
        minlength: 3
      },
      inputemail: {
        required: true,
        email: true
      },
      inputpassword: {
        required: true,
        minlength: 6,
      },
      inputusername: {
        required: true,
        minlength: 5
      },
      inputphone: {
        required: true
      }
    },
    messages:
    {
      inputfirstname: {
        required: "please enter first name",
        minLength: "First name must be atleast 3 characters"
      },
      inputlastname: {
        required: "please enter last name",
        minLength: "Last name must be atleast 3 characters"
      },
      inputemail: {
        required: "Please provide an email address",
        email: "Please enter a valid email address"
      },
      inputpassword:{
        required: "please provide a password",
        minlength: "password at least have 6 characters"
      },
      inputusername: {
        required: "please enter a valid username",
        minlength: "Username must be atleast 5 characters"
      },
      inputphone: {
        required: "please enter your phone" }
    },
    submitHandler: submitForm
  });
  /* handle form submit */
  function submitForm() {
    var data = $("#register-validate").serialize();
    $.ajax({
      type : 'POST',
      url : 'Controllers/RegisterController.php',
      data : data,
      beforeSend: function() {
        $("#error").fadeOut();
        $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span>   sending ...');
      },
      success : function(response) {
          $("#error").fadeIn(1000, function(){
            $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span>   '+data+' !</div>');
            $("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span>   Create Account');
          });

        alert("User Successfully Registered");
        location.href = "?menu=login";
      }
    });
    return false;
  }


  $('#btn-weather').on('click', function() {
    $.ajax({
      type: "POST",
      url: "http://api.openweathermap.org/data/2.5/group?id=1283240,1282898,1283618,7800118&appid=2c550e8363aec113296c30a52a3930d3&units=metric",
      dataType: "json",
      success: function (result, status, xhr) {
        console.log(result);
        res = CreateWeatherJson(result);
        $("#weatherTable").append("<thead><tr><th>City Id</th><th>City Name</th><th>Temperature</th><th>Min Temp</th><th>Max Temp</th><th>Humidity</th><th>Pressure</th></thead></table>");
        $('#weatherTable').DataTable({
          data: JSON.parse(res),
          columns: [
            { data: 'cityId' },
            { data: 'cityName' },
            { data: 'temp' },
            { data: 'tempMin' },
            { data: 'tempMax' },
            { data: 'pressure' },
            { data: 'humidity' }
          ],
        "pageLength": 5
        });
      },
      error: function (xhr, status, error) {
        console.log("Error: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
      }
    });
    return false;
  });

function CreateWeatherJson(json) {
  var newJson = "";
  for (i = 0; i < json.list.length; i++) {
  cityId = json.list[i].id;
  cityName = json.list[i].name;
  temp = json.list[i].main.temp
  pressure = json.list[i].main.pressure
  humidity = json.list[i].main.humidity
  tempmin = json.list[i].main.temp_min
  tempmax = json.list[i].main.temp_max
  newJson = newJson + "{";
  newJson = newJson + "\"cityId\"" + ": " + cityId + ","
  newJson = newJson + "\"cityName\"" + ": " + "\"" + cityName + "\"" + ","
  newJson = newJson + "\"temp\"" + ": " + temp + ","
  newJson = newJson + "\"pressure\"" + ": " + pressure + ","
  newJson = newJson + "\"humidity\"" + ": " + humidity + ","
  newJson = newJson + "\"tempMin\"" + ": " + tempmin + ","
  newJson = newJson + "\"tempMax\"" + ": " + tempmax
  newJson = newJson + "},";
  }
  return "[" + newJson.slice(0, newJson.length - 1) + "]"
  }


  var myIndex = 0;
  Slider();

  function Slider() {
    var i;
    var x = $(".slides");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(Slider, 2000); // Change image every 2 seconds
  }


});